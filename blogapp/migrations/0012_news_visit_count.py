# Generated by Django 2.1.7 on 2019-04-12 02:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blogapp', '0011_auto_20190411_1340'),
    ]

    operations = [
        migrations.AddField(
            model_name='news',
            name='visit_count',
            field=models.PositiveIntegerField(default=0),
        ),
    ]
