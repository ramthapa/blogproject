from django.db import models

# Create your models here.


class Blog(models.Model):
    title = models.CharField(max_length=200)
    image = models.ImageField(upload_to="blogs")
    content = models.TextField()
    author = models.CharField(max_length=200)
    date = models.DateTimeField(auto_now_add=True)
    view_count = models.PositiveIntegerField(default=0)

    def __str__(self):
        # return self.title + "(" + self.author + ")"
        # return self.date
        return str(self.title)


class Comment(models.Model):
    user = models.CharField(max_length=20)
    blog = models.ForeignKey(Blog, on_delete=models.CASCADE)
    message = models.TextField(max_length=200)
    email = models.CharField(max_length=20)

    def __str__(self):
        return self.user


class Event(models.Model):
    date = models.DateTimeField(auto_now_add=True)
    title = models.CharField(max_length=200)
    body = models.TextField()
    details = models.TextField()

    def __str__(self):
        return self.title


class Category(models.Model):
    title = models.CharField(max_length=200)
    image = models.ImageField(upload_to="Category")

    def __str__(self):
        return self.title


class News(models.Model):
    title = models.CharField(max_length=200)
    # category = models.ForeignKey(Category,on_delete = models.CASCADE)
    category = models.ForeignKey(
        Category, on_delete=models.CASCADE, related_name="relatednews")
    image = models.ImageField(upload_to="news")
    detail = models.TextField()
    date = models.DateTimeField(auto_now_add=True)
    author = models.CharField(max_length=100, null=True, blank=True)
    visit_count = models.PositiveIntegerField(default=0)

    def __str__(self):
        return self.title


class Message(models.Model):
    name = models.CharField(max_length=50)
    phone = models.CharField(max_length=10)
    email = models.EmailField(max_length=10, null=True, blank=True)
    subject = models.CharField(max_length=50)
    message = models.TextField()
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name
