from django.urls import path
from .views import *


app_name = "blogapp"

urlpatterns = [
    path("", HomeView.as_view(), name="home"),
    path("about/", AboutView.as_view(), name="about"),
    path("contact/", ContactView.as_view(), name='contact'),
    path("nepal/intro/", NepalView.as_view(), name="Nepal"),
    path("blog/list/", BlogListView.as_view(), name="bloglist"),
    path("event/list/", EventListView.as_view(), name="evenlist"),
    path("news/list/", NewsListView.as_view(), name="evenlist"),
    path("blog/<int:pk>/detail/", BlogDetailView.as_view(), name='blogdetail'),
    path("news/<int:pk>/detail/", NewsDetailView.as_view(), name="newsdetail"),
    path("blog/create/", BlogCreateView.as_view(), name='blogcreate'),
    path("blog/<int:pk>/update/", BlogUpdateView.as_view(), name='blogupdate'),
    path("blog/<int:pk>/delete/", BlogDeleteView.as_view(), name='blogdelete'),
    # path for all news
    path('news/create/', NewsCreateView.as_view(), name='newscreate'),
    path('news/<int:pk>/update/', NewsUpdateView.as_view(), name='newsupdate'),
    path('news/<int:pk>/delete/', NewsDeleteView.as_view(), name='newsdelete'),
    path('login/', LoginView.as_view(), name='login'),
    path("logout/", LogoutView.as_view(), name='logout'),
    path('category/<int:pk>/', CategoryDetailView.as_view(), name='categorydetail'),
    path('user/registration/', UserRegisteration.as_view(), name='userregistration'),
    path('blog/<int:pk>/comment', CommentView.as_view(), name='comment'),
    path("search/", SearchView.as_view(), name='search'),









]
