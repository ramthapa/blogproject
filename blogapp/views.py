from django.views.generic import *
from .models import *
from .forms import *
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render, redirect
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.db.models import Q


class HomeView(TemplateView):
    template_name = "home.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['django'] = " i am django "
        context['blogs'] = Blog.objects.all()
        context['events'] = Event.objects.all()
        context['categorylist'] = Category.objects.all()

        return context


class AboutView(TemplateView):
    template_name = "about.html"


class ContactView(FormView):
    template_name = "contact.html"
    form_class = MessageForm
    success_url = '/'

    def form_valid(self, form):
        sender = form.cleaned_data["sender"]
        mobile = form.cleaned_data["mobile"]
        email = form.cleaned_data["email"]
        subject = form.cleaned_data["subject"]
        message = form.cleaned_data["message"]
        Message.objects.create(name=sender, phone=mobile,
                               email=email, subject=subject, message=message)
        return super().form_valid(form)


class NepalView(TemplateView):
    template_name = "Nepal.html"


class BlogListView(ListView):
    template_name = "bloglist.html"
    queryset = Blog.objects.all()
    #model = Blog
    context_object_name = "allblogs"

    # def count_visit(self):
    # 	if self.visit is not None:
    # 		self.visit += 1
    # 	else:
    # 		self.visit = 0

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['allblogs'] = Blog.objects.all()

        return context


class EventListView(ListView):
    template_name = "eventlist.html"
    queryset = Event.objects.all()
    context_object_name = 'allevents'


class NewsListView(ListView):
    template_name = "newslist.html"
    queryset = News.objects.all()
    context_object_name = "allnews"

# for details of blog


class BlogDetailView(DetailView):
    template_name = "blogdetail.html"
    # queryset = Blog.objects.all()
    model = Blog
    context_object_name = "blogdetail"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['commentform'] = CommentForm
        blog_id = self.kwargs["pk"]
        blog = Blog.objects.get(id=blog_id)
        blog.view_count += 1
        blog.save()
        return context


class NewsDetailView(DetailView):
    template_name = "newsdetail.html"
    model = News
    context_object_name = "newsdetail"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        news_id = self.kwargs['pk']
        news = News.objects.get(id=news_id)
        visit_count = 0
        news.visit_count += 2
        news.save()
        return context

# to create a blog


class BlogCreateView(LoginRequiredMixin, CreateView):
    login_url = '/login/'
    template_name = "blogcreate.html"
    form_class = BlogForm
    success_url = "/"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['blogs'] = Blog.objects.all()

        return context

    def form_valid(self, form):
        title = form.cleaned_data["title"]
        image = form.cleaned_data["image"]
        author = form.cleaned_data["author"]
        content = form.cleaned_data["content"]

        print(title, '_______________')
        return super().form_valid(form)


class BlogUpdateView(LoginRequiredMixin, UpdateView):
    login_url = '/login/'
    template_name = 'blogcreate.html'
    form_class = BlogForm
    success_url = '/blog/list/'
    model = Blog


class BlogDeleteView(LoginRequiredMixin, DeleteView):
    login_url = '/login/'
    template_name = "blogdelete.html"
    model = Blog
    success_url = "/blog/list/"


class NewsCreateView(LoginRequiredMixin, CreateView):
    login_url = '/login/'
    template_name = 'newscreate.html'
    form_class = NewsForm
    success_url = '/news/list/'


class NewsUpdateView(LoginRequiredMixin, UpdateView):
    login_url = '/login/'
    template_name = 'newscreate.html'
    form_class = NewsForm
    model = News
    success_url = "/news/list/"


class NewsDeleteView(LoginRequiredMixin, DeleteView):
    login_url = '/login/'
    template_name = "newsdelete.html"
    model = News
    success_url = '/news/list/'


class LoginView(FormView):
    template_name = 'login.html'
    form_class = LoginForm
    success_url = "/"

    def form_valid(self, form):
        uname = form.cleaned_data["username"]
        pword = form.cleaned_data["password"]

        user = authenticate(username=uname, password=pword)
        if user is not None:
            login(self.request, user)

        else:
            return render(self.request, self.template_name, {
                # 'form':form,
                'error': "you are not authorized"
            })

        return super().form_valid(form)


class LogoutView(View):
    def get(self, request):
        logout(request)
        return redirect("/")


class CategoryDetailView(DetailView):
    template_name = 'categorydetail.html'
    model = Category
    context_object_name = "categorys"


class UserRegisteration(FormView):
    template_name = 'userregistertion.html'
    form_class = UserForm
    success_url = "/login/"

    def form_valid(self, form):
        username = form.cleaned_data["username"]
        email = form.cleaned_data["email"]
        password = form.cleaned_data["password"]
        User.objects.create_user(username, email, password)

        return super().form_valid(form)

# comment


class CommentView(CreateView):
    template_name = 'comment.html'
    form_class = CommentForm
    success_url = '/'

    def form_valid(self, form):
        blog_id = self.kwargs['pk']
        blog = Blog.objects.get(id=blog_id)
        form.instance.blog = blog

        return super().form_valid(form)

    def get_succcess_url(form):
        blog_id = self.kwargs['pk']
        url = "/blog/" + str(blog_id) + "/detail/"
        return url


class SearchView(TemplateView):
    template_name = 'searchresult.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        keyword = self.request.GET.get("search")
        blogs = Blog.objects.filter(
            Q(title__icontains=keyword) | Q(content__icontains=keyword))
        news = News.objects.filter(Q(title__icontains=keyword) | Q(
            category__title__icontains=keyword))
        context['searchednews'] = news
        context['searchedblogs'] = blogs

        return context
